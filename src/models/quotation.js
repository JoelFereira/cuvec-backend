const connection = require('../database')

let quotationModel = {}

quotationModel.createQuotation = (callback, req) => {
    if (connection) {
        const lastQuotation = `SELECT id_quotation FROM quotation ORDER BY id_quotation DESC LIMIT 1`
        const lastClient = `SELECT id_client FROM client ORDER BY id_client DESC LIMIT 1`
        const existQuery = `SELECT id_person FROM person WHERE (rut_person = '${req.client.rut_person}')`
        const addPerson = `INSERT INTO person (rut_person, first_name, second_name, p_last_name, m_last_name, person_type, id_comuna, address, phone, email) VALUES ('${req.client.rut_person}', '${req.client.first_name}', '${req.client.second_name}', '${req.client.p_last_name}', '${req.client.m_last_name}', 2, 1, '${req.client.address}', '${req.client.phone}', '${req.client.email}')`
        let addClient = (id) => `INSERT INTO client (id_person) VALUES (${id})`
        let existClient = (id) => `SELECT * FROM client WHERE (id_person = '${id}')`
        let addQuotation = (id) => `INSERT INTO quotation (id_client, id_comune, address, p_length, p_weigth, p_min_depth, p_max_depth, total) VALUES ('${id}', 1, '-', '${req.medidas.lar}', '${req.medidas.ach}', '${req.medidas.pmin}', '${req.medidas.pmax}', '${req.total}')`
        let addMaterials = (id_material, id_quotation, qty, moment_price) => `INSERT INTO quotation_material (id_material, id_quotation, qty, moment_price) VALUES ('${id_material}', '${id_quotation}', '${qty}', '${moment_price}')`        

        const create = (id_client, materials) => {
            
        } 

        connection.query(existQuery, (err, rows) => {
            if (err) throw err;
            if (rows.length > 0) {
                connection.query(existClient(rows[0].id_person), (err2, rows2) => {
                    if (err2) throw err2;
                    if (rows2.length == 0) {
                        connection.query(addClient(rows[0].id_person), (err3, rows3) => {
                            if (err3) throw err3;
                            connection.query(lastClient, (err4, rows4) => {
                                if (err4) throw err4;
                                connection.query(addQuotation(rows4[0].id_client),(err5, rows5) => {
                                    if (err5) throw err5;
                                    connection.query(lastQuotation, (err6, rows6) => {
                                        if (err6) throw err6;
                                        req.materials.forEach(e => {
                                            connection.query(addMaterials(e.id_material, rows6[0].id_quotation, e.qty, e.moment_price), (err7, rows7) => {
                                                if (err7) throw err7;
                                            })
                                        });
                                        callback(null, 'Cotización creada')
                                    })
                                })
                            })
                        })
                    } else {
                        connection.query(addQuotation(rows2[0].id_client),(err3, rows3) => {
                            if (err3) throw err3;
                            connection.query(lastQuotation, (err4, rows4) => {
                                if (err4) throw err4;
                                req.materials.forEach(e => {
                                    connection.query(addMaterials(e.id_material, rows4[0].id_quotation, e.qty, e.moment_price), (err5, rows5) => {
                                        if (err5) throw err5;
                                    })
                                });
                                callback(null, 'Cotización creada')
                            })
                        })
                    }
                })
            } else {
                connection.query(addPerson, (err2, rows2) => {
                    if (err2) throw err2;
                    connection.query(existQuery, (err3, rows3) => {
                        if (err3) throw err3;
                        connection.query(addClient(rows3[0].id_person), (err4, rows4) => {
                            if (err4) throw err4;
                            connection.query(lastClient, (err5, rows5) => {
                                if (err5) throw err5;
                                connection.query(addQuotation(rows5[0].id_client),(err6, rows6) => {
                                    if (err6) throw err6;
                                    connection.query(lastQuotation, (err7, rows7) => {
                                        if (err7) throw err7;
                                        req.materials.forEach(e => {
                                            connection.query(addMaterials(e.id_material, rows7[0].id_quotation, e.qty, e.moment_price), (err8, rows8) => {
                                                if (err8) throw err8;
                                            })
                                        });
                                        callback(null, 'Cotización creada')
                                    })
                                })
                            })
                        })
                    })
                })
            }
        })
    }
}

quotationModel.getQuotations = (callback) => {
    if (connection) {
        const query = `SELECT * FROM quotation INNER JOIN client ON client.id_client = quotation.id_client INNER JOIN quotation_material ON quotation_material.id_quotation = quotation.id_quotation INNER JOIN material ON quotation_material.id_material = material.id_material INNER JOIN person ON person.id_person = client.id_person`
        connection.query(query, (err, rows) => {
            if (err) throw err;
            if(rows.length > 0) {
                callback(null, rows);
            } else {
                callback('No se encuentran cotizaciones', null)
            }
        })
    }
}

quotationModel.updateQuotation = (callback, req) => {
    if (connection) {
        const query = `UPDATE person SET rut_person = '${req.rut_person}', first_name = '${req.first_name}', id_comuna = 1, address = '${req.address}', phone = '${req.phone}', email = '${req.email}' WHERE id_person = '${req.id_person}'`
        connection.query(query, (err, rows) => {
            if (err) callback('Error al actualizar', null);
            callback(null, 'Proveedor actualizado')
        })
    }
}

quotationModel.getClients = (callback) => {
    if (connection) {
        const query = "SELECT * FROM client INNER JOIN person ON person.id_person = client.id_person"
        connection.query(query, (err, rows) => {
            if (err) callback('No se encuentran clientes', null);
            callback(null, rows)
        })
    }
}


module.exports = quotationModel