const connection = require('../database')

let providersModel = {}

providersModel.createProvider = (callback, req) => {
    if (connection) {
        const existQuery = `SELECT id_person FROM person WHERE (rut_person = '${req.rut_person}')`
        const addPerson = `INSERT INTO person (rut_person, first_name, person_type, id_comuna, address, phone, email) VALUES ('${req.rut_person}', '${req.first_name}', 2, 1, '${req.address}', '${req.phone}', '${req.email}')`
        let addProvider = (id) => `INSERT INTO provider (id_person) VALUES (${id})`
        let existProvider = (id) => `SELECT * FROM provider WHERE (id_person = '${id}')`

        connection.query(existQuery, (err, rows) => {
            if (err) throw err;
            console.log(rows)
            if (rows.length > 0) {
                connection.query(existProvider(rows[0].id_person), (err, rows2) => {
                    if (err) throw err;
                    if (rows2.length == 0) {
                        connection.query(addProvider(rows[0].id_person), (err, rows3) => {
                            if (err) throw err;
                            callback(null, 'Proveerdor creado')
                        })
                    } else {
                        callback(null, 'Proveerdor ya existente')
                    }
                })
            } else {
                connection.query(addPerson, (err, rows2) => {
                    if (err) throw err;
                    connection.query(existQuery, (err, rows3) => {
                        if (err) throw err;
                        connection.query(addProvider(rows3[0].id_person), (err, rows4) => {
                            if (err) throw err;
                            callback(null, rows4)
                        })
                    })
                })
            }
        })
    }
}

providersModel.getProviders = (callback) => {
    if (connection) {
        const query = `SELECT person.id_person, rut_person, first_name, second_name, p_last_name, m_last_name, id_comuna, address, phone, email, person.status AS 'personStatus', id_provider, provider.status AS 'providerStatus'  FROM person INNER JOIN provider ON provider.id_person = person.id_person`
        connection.query(query, (err, rows) => {
            if (err) throw err;
            if(rows.length > 0) {
                callback(null, rows);
            } else {
                callback('No se encuentran proveedores', null)
            }
        })
    }
}

providersModel.updateProvider = (callback, req) => {
    if (connection) {
        const query = `UPDATE person SET rut_person = '${req.rut_person}', first_name = '${req.first_name}', id_comuna = 1, address = '${req.address}', phone = '${req.phone}', email = '${req.email}' WHERE id_person = '${req.id_person}'`
        connection.query(query, (err, rows) => {
            if (err) callback('Error al actualizar', null);
            callback(null, 'Proveedor actualizado')
        })
    }
}


module.exports = providersModel