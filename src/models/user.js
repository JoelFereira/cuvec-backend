const connection = require('./../database')

let userModel = {}

userModel.getUsers = (callback, req) => {
    if (connection) {
        const existQuery = `SELECT * FROM user INNER JOIN person ON user.id_person = person.id_person WHERE (user.username = '${req.user}' OR person.email='${req.user}')`

        connection.query(existQuery, (err, rows) => {
            if (err) throw err;
            if (rows.length > 0) {

                if (rows[0].pass == req.pass) {
                    callback(null, rows)
                } else {
                    callback('Contraseña incorrecta', null)
                }
            } else {
                callback('Usuario no existe', null)
            }
        })
    }
}

module.exports = userModel