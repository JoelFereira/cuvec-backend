const connection = require('../database')

let materialsModel = {}

materialsModel.createMaterial = (callback, req) => {
    if (connection) {
        const lastMaterial = `SELECT id_material FROM material ORDER BY id_material DESC LIMIT 1`
        const addMaterial = `INSERT INTO material (name_material, aggregating, formule, unidad) VALUES ('${req.name_material}', '${req.aggregating}', '${req.formule}', '${req.unidad}')`
        const addMaterialProvider = (idMaterial) => `INSERT INTO provider_materials (id_material, id_provider, price_material) VALUES ('${idMaterial}', '${req.id_provider}', '${req.price_material}')`

        connection.query(addMaterial, (err, rows) => {
            if (err) throw err;
            connection.query(lastMaterial, (err, rows2) => {
                if (err) throw err;
                connection.query(addMaterialProvider(rows2[0].id_material), (err, rows3) => {
                    if (err) throw err;
                    callback(null, 'Material agregado')
                })
            })
        })
    }
}

materialsModel.getMaterials = (callback) => {
    if (connection) {
        const query = `SELECT material.name_material, material.unidad, material.id_material, provider.id_provider, provider_materials.price_material, person.first_name, person.rut_person, material.formule, material.aggregating, material.status FROM provider_materials LEFT JOIN material ON material.id_material = provider_materials.id_material RIGHT JOIN provider ON provider.id_provider = provider_materials.id_provider RIGHT JOIN person ON person.id_person = provider.id_person`
        connection.query(query, (err, rows) => {
            if (err) throw err;
            if(rows.length > 0) {
                callback(null, rows.filter(e => e.id_material != null));
            } else {
                callback('No se encuentran materiales', null)
            }
        })
    }
}

materialsModel.getActiveMaterials = (callback) => {
    if (connection) {
        const query = `SELECT material.name_material, material.unidad, material.id_material, provider_materials.price_material, material.formule, material.aggregating, material.status FROM provider_materials LEFT JOIN material ON material.id_material = provider_materials.id_material WHERE material.status = 1`
        connection.query(query, (err, rows) => {
            if (err) throw err;
            if(rows.length > 0) {
                callback(null, rows.filter(e => e.id_material != null));
            } else {
                callback('No se encuentran materiales activos', null)
            }
        })
    }
}

materialsModel.updateMaterialState = (callback, req) => {
    if (connection) {
        const query = `UPDATE material SET status = '${req.status}' WHERE id_material = '${req.id_material}'`
        connection.query(query, (err, rows) => {
            if (err) callback('Error al actualizar', null);
            callback(null, 'Material actualizado')
        })
    }
}

materialsModel.updateMaterial = (callback, req) => {
    if (connection) {
        const updateMaterial = `UPDATE material SET name_material = '${req.name_material}', aggregating = '${req.aggregating}', formule ='${req.formule}', unidad = '${req.unidad}' WHERE id_material = '${req.id_material}'`
        const updateMaterialProvider = `UPDATE provider_materials SET id_provider = '${req.id_provider}', price_material = '${req.price_material}' WHERE id_material = '${req.id_material}'`
        connection.query(updateMaterial, (err, rows) => {
            if (err) callback('Error al actualizar', null);
            connection.query(updateMaterialProvider, (err2, rows2) => {
                if (err2) callback('Error al actualizar', null);
                callback(null, 'Material actualizado')
            })
        })
    }
}


module.exports = materialsModel