-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-01-2020 a las 01:34:26
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cuvec`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

CREATE TABLE `client` (
  `id_client` int(11) NOT NULL,
  `id_person` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `client`
--

INSERT INTO `client` (`id_client`, `id_person`, `create_date`, `update_date`) VALUES
(1, 5, '2019-09-27 08:06:41', '2019-09-27 08:06:41'),
(2, 6, '2019-09-27 08:30:57', '2019-09-27 08:30:57'),
(3, 2, '2019-09-27 14:35:29', '2019-09-27 14:35:29'),
(4, 7, '2019-09-27 15:00:27', '2019-09-27 15:00:27'),
(5, 8, '2019-09-30 19:18:59', '2019-09-30 19:18:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comune`
--

CREATE TABLE `comune` (
  `id_comune` int(11) NOT NULL,
  `name_comune` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `comune`
--

INSERT INTO `comune` (`id_comune`, `name_comune`, `create_date`, `update_date`) VALUES
(1, 'LAS CONDES', '2019-08-19 20:20:39', '2019-08-19 20:20:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `material`
--

CREATE TABLE `material` (
  `id_material` int(11) NOT NULL,
  `name_material` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aggregating` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formule` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unidad` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `create_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `material`
--

INSERT INTO `material` (`id_material`, `name_material`, `aggregating`, `formule`, `unidad`, `status`, `create_date`, `update_date`) VALUES
(5, 'Material de Prueba', '0', '', 'Unidad', '1', '2019-09-27 00:01:13', '2019-09-27 16:01:15'),
(6, 'Material', '1', '(LAR*4)/2', 'Camion', '1', '2019-09-27 00:08:52', '2019-11-18 21:45:02'),
(7, 'Material Actualizado', '1', '(LAR*6)/2', 'Unidad', '1', '2019-09-27 16:18:09', '2019-09-27 16:18:09'),
(8, 'Material B', '1', '(LAR*2)*2', 'Saco', '2', '2019-09-30 19:16:17', '2019-11-18 21:45:14'),
(9, 'Cemento', '1', 'VOL*3', 'Saco', '1', '2019-11-18 21:47:29', '2019-11-18 21:52:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE `person` (
  `id_person` int(11) NOT NULL,
  `rut_person` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `second_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_last_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `m_last_name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_type` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `id_comuna` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `create_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `person`
--

INSERT INTO `person` (`id_person`, `rut_person`, `first_name`, `second_name`, `p_last_name`, `m_last_name`, `person_type`, `id_comuna`, `address`, `phone`, `email`, `status`, `create_date`, `update_date`) VALUES
(1, '27.093.422', 'JOEL', 'ENRIQUE', 'FEREIRA', 'BALZAN', '1', 1, 'AV. ALEJANDRO FLEMING #9330', '+56 9 5683 0926', 'joelfereira15@gmail.com', '1', '2019-08-19 20:26:11', '2019-08-19 20:26:11'),
(2, '123456789', 'Nombre de Prueba', NULL, NULL, NULL, '2', 1, 'Calle de Prueba', '1145555', '', '1', '2019-09-26 10:18:12', '2019-09-26 11:33:48'),
(3, '147852369', 'Proveedor de Prueba', NULL, NULL, NULL, '2', 1, 'Dirección de Prueba', '+58 412 649 6817', 'example@example.com', '1', '2019-09-26 12:48:51', '2019-09-26 12:55:18'),
(4, '963258741', 'Proveedor de Prueba2', NULL, NULL, NULL, '2', 1, 'Dirección de Prueba', '', 'example2@example.com', '1', '2019-09-26 12:55:00', '2019-09-26 13:16:32'),
(5, '785694123', 'Cliente de Prueba', '', '', '', '2', 1, 'Dirección de Prueba', '', '', '1', '2019-09-27 08:06:41', '2019-09-27 08:06:41'),
(6, '123654789', 'Cliente 2 de Prueba', '', 'Apellido Paterno', '', '2', 1, 'Su Casa', '', '', '1', '2019-09-27 08:30:56', '2019-09-27 08:30:56'),
(7, '852147963', 'Cliente Comercial', '', '', '', '2', 1, 'Una tienda', '', '', '1', '2019-09-27 15:00:27', '2019-09-27 15:00:27'),
(8, '1231245135', 'adfadf', '', '', '', '2', 1, 'adsfadf', '', '', '1', '2019-09-30 19:18:58', '2019-09-30 19:18:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provider`
--

CREATE TABLE `provider` (
  `id_provider` int(11) NOT NULL,
  `id_person` int(11) DEFAULT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `create_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `provider`
--

INSERT INTO `provider` (`id_provider`, `id_person`, `status`, `create_date`, `update_date`) VALUES
(6, 3, '1', '2019-09-26 12:48:51', '2019-09-26 12:48:51'),
(7, 4, '1', '2019-09-26 12:55:00', '2019-09-26 12:55:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provider_materials`
--

CREATE TABLE `provider_materials` (
  `id` int(11) NOT NULL,
  `id_provider` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `price_material` double(10,2) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `provider_materials`
--

INSERT INTO `provider_materials` (`id`, `id_provider`, `id_material`, `price_material`, `create_date`, `update_date`) VALUES
(3, 7, 5, 5000.00, '2019-09-27 00:01:13', '2019-09-27 00:01:13'),
(4, 6, 6, 1000.00, '2019-09-27 00:08:52', '2019-09-27 00:08:52'),
(5, 6, 7, 1000.00, '2019-09-27 16:18:09', '2019-09-27 16:18:09'),
(6, 6, 8, 5000.00, '2019-09-30 19:16:17', '2019-09-30 19:16:17'),
(7, 6, 9, 1000.00, '2019-11-18 21:47:29', '2019-11-18 21:47:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quotation`
--

CREATE TABLE `quotation` (
  `id_quotation` int(11) NOT NULL,
  `id_comune` int(11) NOT NULL,
  `id_client` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `p_length` double(10,2) NOT NULL,
  `p_weigth` double(10,2) NOT NULL,
  `p_min_depth` double(10,2) NOT NULL,
  `p_max_depth` double(10,2) NOT NULL,
  `total` double(10,2) NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `create_date` timestamp NULL DEFAULT current_timestamp(),
  `update_date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `quotation`
--

INSERT INTO `quotation` (`id_quotation`, `id_comune`, `id_client`, `address`, `p_length`, `p_weigth`, `p_min_depth`, `p_max_depth`, `total`, `status`, `create_date`, `update_date`) VALUES
(1, 1, 1, '-', 10.00, 15.00, 1.50, 0.00, 70000.00, '1', '2019-09-27 08:25:14', '2019-09-27 08:25:14'),
(2, 1, 2, '-', 5.00, 8.00, 1.80, 0.00, 20000.00, '1', '2019-09-27 08:31:48', '2019-09-27 08:31:48'),
(3, 1, 3, '-', 10.00, 8.00, 1.50, 0.00, 30000.00, '1', '2019-09-27 14:41:56', '2019-09-27 14:41:56'),
(4, 1, 4, '-', 4.00, 5.00, 1.50, 1.70, 18000.00, '1', '2019-09-27 15:00:28', '2019-09-27 15:00:28'),
(5, 1, 5, '-', 5.00, 4.00, 1.50, 1.90, 150000.00, '1', '2019-09-30 19:18:59', '2019-09-30 19:18:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `quotation_material`
--

CREATE TABLE `quotation_material` (
  `id` int(11) NOT NULL,
  `id_quotation` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `moment_price` double(10,2) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `quotation_material`
--

INSERT INTO `quotation_material` (`id`, `id_quotation`, `id_material`, `qty`, `moment_price`, `create_date`, `update_date`) VALUES
(3, 2, 5, 2, 10000.00, '2019-09-27 08:31:48', '2019-09-27 08:31:48'),
(4, 2, 6, 10, 10000.00, '2019-09-27 08:31:48', '2019-09-27 08:31:48'),
(5, 3, 5, 2, 10000.00, '2019-09-27 14:41:56', '2019-09-27 14:41:56'),
(6, 3, 6, 20, 20000.00, '2019-09-27 14:41:56', '2019-09-27 14:41:56'),
(7, 4, 5, 2, 10000.00, '2019-09-27 15:00:28', '2019-09-27 15:00:28'),
(8, 4, 6, 8, 8000.00, '2019-09-27 15:00:28', '2019-09-27 15:00:28'),
(9, 5, 5, 5, 25000.00, '2019-09-30 19:18:59', '2019-09-30 19:18:59'),
(10, 5, 6, 10, 10000.00, '2019-09-30 19:18:59', '2019-09-30 19:18:59'),
(11, 5, 7, 15, 15000.00, '2019-09-30 19:18:59', '2019-09-30 19:18:59'),
(12, 5, 8, 20, 100000.00, '2019-09-30 19:18:59', '2019-09-30 19:18:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `id_person` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `create_date` timestamp NULL DEFAULT current_timestamp(),
  `update_date` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id_user`, `id_person`, `username`, `pass`, `status`, `create_date`, `update_date`) VALUES
(1, 1, 'Calmizir', 'admin', '1', '2019-08-19 20:31:07', '2019-08-19 20:31:07');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_client`) USING BTREE,
  ADD KEY `person_quotation` (`id_person`) USING BTREE;

--
-- Indices de la tabla `comune`
--
ALTER TABLE `comune`
  ADD PRIMARY KEY (`id_comune`) USING BTREE;

--
-- Indices de la tabla `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id_material`) USING BTREE;

--
-- Indices de la tabla `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`id_person`) USING BTREE,
  ADD UNIQUE KEY `rut` (`rut_person`) USING BTREE,
  ADD KEY `comune_person` (`id_comuna`) USING BTREE;

--
-- Indices de la tabla `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`id_provider`) USING BTREE,
  ADD KEY `id_person_provider` (`id_person`) USING BTREE;

--
-- Indices de la tabla `provider_materials`
--
ALTER TABLE `provider_materials`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `provider_material` (`id_provider`) USING BTREE,
  ADD KEY `material_provider` (`id_material`) USING BTREE;

--
-- Indices de la tabla `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id_quotation`) USING BTREE,
  ADD KEY `comune_quotation` (`id_comune`) USING BTREE,
  ADD KEY `client_cuotation` (`id_client`);

--
-- Indices de la tabla `quotation_material`
--
ALTER TABLE `quotation_material`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `quotation_material` (`id_quotation`) USING BTREE,
  ADD KEY `material_quotation` (`id_material`) USING BTREE;

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`) USING BTREE,
  ADD KEY `id_person_user` (`id_person`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `client`
--
ALTER TABLE `client`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `comune`
--
ALTER TABLE `comune`
  MODIFY `id_comune` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `material`
--
ALTER TABLE `material`
  MODIFY `id_material` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `person`
--
ALTER TABLE `person`
  MODIFY `id_person` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `provider`
--
ALTER TABLE `provider`
  MODIFY `id_provider` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `provider_materials`
--
ALTER TABLE `provider_materials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id_quotation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `quotation_material`
--
ALTER TABLE `quotation_material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `person_quotation` FOREIGN KEY (`id_person`) REFERENCES `person` (`id_person`);

--
-- Filtros para la tabla `person`
--
ALTER TABLE `person`
  ADD CONSTRAINT `comune_person` FOREIGN KEY (`id_comuna`) REFERENCES `comune` (`id_comune`);

--
-- Filtros para la tabla `provider`
--
ALTER TABLE `provider`
  ADD CONSTRAINT `id_person_provider` FOREIGN KEY (`id_person`) REFERENCES `person` (`id_person`);

--
-- Filtros para la tabla `provider_materials`
--
ALTER TABLE `provider_materials`
  ADD CONSTRAINT `material_provider` FOREIGN KEY (`id_material`) REFERENCES `material` (`id_material`),
  ADD CONSTRAINT `provider_material` FOREIGN KEY (`id_provider`) REFERENCES `provider` (`id_provider`);

--
-- Filtros para la tabla `quotation`
--
ALTER TABLE `quotation`
  ADD CONSTRAINT `client_cuotation` FOREIGN KEY (`id_client`) REFERENCES `client` (`id_client`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `comune_quotation` FOREIGN KEY (`id_comune`) REFERENCES `comune` (`id_comune`);

--
-- Filtros para la tabla `quotation_material`
--
ALTER TABLE `quotation_material`
  ADD CONSTRAINT `material_quotation` FOREIGN KEY (`id_material`) REFERENCES `material` (`id_material`),
  ADD CONSTRAINT `quotation_material` FOREIGN KEY (`id_quotation`) REFERENCES `quotation` (`id_quotation`);

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `id_person_user` FOREIGN KEY (`id_person`) REFERENCES `person` (`id_person`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
