const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cors = require('cors')

// Initializations
const app = express()

// Settings
app.set('port', process.env.PORT || 4000);

// Middlewares
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors());

/* app.use((req, res) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header('Access-Control-Allow-Credentials', 'true');
}) */

//Routes
require('./routes/userRoutes')(app);
require('./routes/providersRoutes')(app);
require('./routes/materialsRoutes')(app);
require('./routes/quotationsRoutes')(app);

app.listen(app.get('port'), () => {
    console.log('Server Running in:', app.get('port'))
});
