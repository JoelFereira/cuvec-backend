const quotations = require('./../models/quotation')

module.exports = function (app) {
    app.post('/create/quotation', (req, res) => {
        quotations.createQuotation((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        }, req.body)
    }),
    app.get('/quotations', (req, res) => {
        quotations.getQuotations((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        })
    }),
    app.get('/clients', (req, res) => {
        quotations.getClients((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        })
    }),
    app.put('/update/quotation', (req, res) => {
        quotations.updateQuotation((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        }, req.body)
    })
}