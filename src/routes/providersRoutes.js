const providers = require('./../models/providers')

module.exports = function (app) {
    app.post('/create/provider', (req, res) => {
        providers.createProvider((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        }, req.body)
    }),
    app.get('/provider', (req, res) => {
        providers.getProviders((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        })
    }),
    app.put('/update/provider', (req, res) => {
        providers.updateProvider((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        }, req.body)
    })
}