const materials = require('./../models/materials')

module.exports = function (app) {
    app.post('/create/material', (req, res) => {
        materials.createMaterial((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        }, req.body)
    }),
    app.get('/materials', (req, res) => {
        materials.getMaterials((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        })
    }),
    app.get('/active/materials', (req, res) => {
        materials.getActiveMaterials((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        })
    }),
    app.put('/update/material/status', (req, res) => {
        materials.updateMaterialState((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        }, req.body)
    }),
    app.put('/update/material', (req, res) => {
        materials.updateMaterial((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        }, req.body)
    })
}