const user = require('./../models/user')

module.exports = function (app) {
    app.post('/login/', (req, res) => {
        user.getUsers((err, data) => {
            if (err) {
                res.status(404).json({
                    err: true,
                    msg: err
                })
            } else {
                res.status(200).json(data)
            }
        }, req.body)
    })
}